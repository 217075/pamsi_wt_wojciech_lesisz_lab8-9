// ConsoleApplication19.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include<algorithm>
#include<cmath>
#include<cstdlib>
#include<ctime>
#include<iostream>
#include <fstream>
#include<vector>
#include <string>
#include <algorithm>
#include <ctime>
#define MAX 10000
using namespace std;
class Wierzcholek {
public:
	int W;
	int nastepny;
	int poprzedni;
	int D; // drzewo
	Wierzcholek() {
		W = 0;
		nastepny = 0;
		poprzedni = 0;

	}
	 Wierzcholek(int w, int n, int p) {
		W = w;
		nastepny = n;
		poprzedni = p;
	}
	void Wierzcholek2(int w, int n, int p) {
		W = w;
		nastepny = n;
		poprzedni = p;
	}
	int WyswietlW(){
		return W;
	}
};
class Krawedz {
public:
	int numerkr;
	int p_init;
	int p_final;
	int waga;
	int drzewo;
	Krawedz()
	{
		p_init = NULL;
		p_final = NULL;
		waga = -1;
		drzewo = 0;
	}
	Krawedz(int w1, int w2, int w)
	{
		p_init = w1;
		p_final = w2;
		waga = w;
		//		return w;
	}

};
std::vector<Krawedz*> lista_krawedzi;
std::vector<Wierzcholek*> lista_wierzcholkow;
std::vector<Wierzcholek*> lista_wierzcholkow_p;
std::vector<Wierzcholek*> lista_wierzcholkow_prim;

std::vector<Krawedz*> lista_krawedzi_krusk;
std::vector<Krawedz*> lista_krawedzi_krusk_temp;


std::vector<Krawedz*> lista_prim;
std::vector<Krawedz*> lista_prim2;

static int M_D = 0;


double obliczSekundy(clock_t czas)
{
	return static_cast <double>(czas) / CLOCKS_PER_SEC;
}


class Graf {
public:
	int liczba_w;
	int liczba_k;
	void liczk(float gestosc) {
		liczba_k = gestosc / 100 * liczba_w*(liczba_w - 1) / 2;
	}
	int **graf; 

	int wmax = 0;
	class TNode
	{
		public:
		int node;            // numer wierzcho�ka
		int weight;          // waga kraw�dzi
		TNode * next; // nast�pny element listy
	};

	TNode **L;
	void LS() {
		L = new TNode*[liczba_w];
	//	for (int i = 0; i < liczba_w; i++)
		//	L[i] = NULL;
	}


	
	void ListaS() {
		TNode *p;
		int x, y, z;
		for (int i = 0; i < liczba_w; i++)
			L[i] = NULL;
		for (int i = 0; i < liczba_k; i++)
		{

			x = (rand() % liczba_k);
			y = (rand() % liczba_k);
			z = (rand() % MAX+ 1);
			wmax = (x > wmax) ? x : wmax;
			wmax = (y > wmax) ? y : wmax;
			p = new TNode;
			p->next = L[x - 1];
			p->node = y; 
			p->weight = z;
			L[x - 1] = p;

			 p = new TNode;
			p->next = L[y - 1];
			p->node = x; 
			p->weight = z;
			L[y - 1] = p;
		}

	}
	void ListaSW() {
		TNode *p;
		for (int i = 0; i < wmax; i++)
		{
			cout << i + 1 << ":";
			p = L[i];
			while (p)
			{
				cout << p->node << "*" << p->weight << "  ";
				p = p->next;
			}
			cout << endl;
		}
	}

	void tablica() {
		graf = new int*[liczba_w];
		for (int i = 0; i < liczba_w; i++)
			graf[i] = new int[liczba_w];
	}

	void zeruj() {
		for (int i = 0; i < liczba_w; i++) {
			for (int j = 0; j < liczba_w; j++)
				graf[i][j] = 0;
		}

	}



	Krawedz* dodajKrawedz(Krawedz* k)
	{
		k->numerkr = lista_krawedzi.size();
		lista_krawedzi.push_back(k);
		
		return k;
	}
	Krawedz* dodajKrawedz2(Krawedz* k)
	{
		k->numerkr = lista_krawedzi_krusk_temp.size();
		lista_krawedzi_krusk_temp.push_back(k);
		return k;
	}

	Krawedz* SortujKrawedzie(int start, int koniec)
	{
		sort(lista_krawedzi.begin(), lista_krawedzi.end());
		return NULL;
	}

	void wypelnij() {

		int n = 0;
		srand(time(NULL));

		for (int i = 0; i < liczba_w; i++) {

			n = 0;
			while (n == 1) {
				int j = rand() % liczba_w;

				if (graf[i][j] == 0 && i != j)
				{
					graf[i][j] = graf[j][i] = (rand() % MAX) + 1;
					Krawedz* temp = new Krawedz(i, j, graf[i][j]);
					Krawedz* temp2 = new Krawedz(i, j, graf[i][j]);
					dodajKrawedz(temp);
					dodajKrawedz2(temp2);
					n = 1;
				}
			}
		}
		while (n < liczba_k) {
			int j = rand() % liczba_w;
			int i = rand() % liczba_w;
			if (graf[i][j] == 0 && i != j)
			{
				graf[i][j] = graf[j][i] = (rand() % MAX) + 1;
				Krawedz* temp = new Krawedz(i, j, graf[i][j]);
				Krawedz* temp2 = new Krawedz(i, j, graf[i][j]);
				dodajKrawedz(temp);
				dodajKrawedz2(temp2);
				n++;
			}
		}
	}

	void wyswietl() {
		int ilo = 0;
		for (int i = 0; i < liczba_w; i++) {
			cout << "\n";
			for (int j = 0; j < liczba_w; j++) {
				if (graf[i][j] != 0) {
					ilo++;
					cout << i << "." << j << "." << graf[i][j] << "  ";
				}
			}
		}
		cout << "\n";
	}



	void zapisz() {
		string txtplik;
		cout << "Podaj nazwe pliku: ";
		cin >> txtplik;
		txtplik += ".txt";
		ofstream txt(txtplik); 
		for (int i = 0; i < liczba_w; i++) {
			for (int j = 0; j < liczba_w; j++) {
				txt << graf[i][j] << "\t";
			}
			txt << "\n";
		}
		txt.close();

	}
	int wczytaj() {
		int liczba;
		int l_kolumn = 0, l_wierszy = 0;
		string txtpliko;
		string linia;
		cout << "Podaj nazwe pliku: ";
		cin >> txtpliko;
		txtpliko += ".txt"; 
		fstream txtcount(txtpliko); 
		if (txtcount.good() == false) {
			cout << "B��d! Z�a nazwa pliku!" << endl;
			return 0;
		}
		while (getline(txtcount, linia)) {
			l_wierszy++; //cout << linia << endl;
			for (int i = 0; i <= linia.length(); i++) {
				if (linia[i] == '\t' || linia[i] == ' ') l_kolumn++;
			}
		}
		l_kolumn = l_kolumn / l_wierszy;
		cout << "ilosc wierszy " << l_wierszy << " ilosc kolumn " << l_kolumn << endl;
		liczba_w = l_wierszy;
		tablica();
		zeruj();
		fstream txtread(txtpliko); 
		for (int i = 0; i < liczba_w; ++i) {
			for (int j = 0; j < liczba_w; ++j) {
				txtread >> liczba;
				if (graf[i][j] == 0 && i != j)
				{
					graf[i][j] = graf[j][i] = liczba;
					if (graf[i][j] != 0) {
						Krawedz* temp = new Krawedz(i, j, graf[i][j]);
						Krawedz* temp2 = new Krawedz(i, j, graf[i][j]);
						dodajKrawedz(temp);
						dodajKrawedz2(temp2);
					}
				}
			}


		}
		return liczba_w;
	}
};


void quick2(int start, int koniec)
{
	Krawedz *srodek = (lista_krawedzi[(start + koniec) / 2]);
	int i = start, j = koniec, k = start, l = 0;
	Krawedz *x = (lista_krawedzi[(start + koniec) / 2]);
	(lista_krawedzi[(start + koniec) / 2]) = (lista_krawedzi[koniec]);
	(lista_krawedzi[koniec]) = x;
	while (i <= j) {
		if (lista_krawedzi[i]->waga < srodek->waga) {
			x = lista_krawedzi[k];
			lista_krawedzi[k] = lista_krawedzi[i];

			lista_krawedzi[i] = x;
			k++;
			i++;
		}
		else { i++; }
	}
	x = lista_krawedzi[k];
	lista_krawedzi[k] = srodek;
	lista_krawedzi[koniec] = x;
	k++;
	l = k;
	i = start;
	while (i <= j) {
		if (lista_krawedzi[i]->waga > srodek->waga) {
			lista_krawedzi[l] = lista_krawedzi[i];
			l++;
			i++;
		}
		else { i++; }
	}
	l = l - k;

	if (start < j && k - start >= 1)
	{
		quick2(start, k - 1);
	}

	if (k - 1 <= koniec && l > 0) {
		quick2(k, koniec);
	}


}



bool listawput(Krawedz t)
{
	int a, b, c;
	int wt;
	int wt2;
	int temp = 0;
	int temp2 = 0;
	if (lista_wierzcholkow.empty() != 1) {
		for (int i = 0; i < lista_wierzcholkow.size(); i++) {
			if (lista_wierzcholkow[i]->W == t.p_final) {
				temp = 1;
				wt = lista_wierzcholkow[i]->D;
				a = i;
			}
			if (lista_wierzcholkow[i]->W == t.p_init) {
				temp2 = 1;
				wt2 = lista_wierzcholkow[i]->D;
				b = i;
			}
		}
	}
	if (temp == 0 && temp2 == 0) {

		M_D++;
		Wierzcholek *temp3 = new Wierzcholek();
		temp3->W = t.p_final;
		temp3->D = M_D;
		lista_wierzcholkow.push_back(temp3);

		Wierzcholek *temp4 = new Wierzcholek();
		temp4->W = t.p_init;
		temp4->D = M_D;
		lista_wierzcholkow.push_back(temp4);
		t.drzewo = M_D;
		return 1;
	}


	if (temp == 0) {
		Wierzcholek *temp3 = new Wierzcholek();
		temp3->W = t.p_final;
		temp3->D = lista_wierzcholkow[b]->D;
		t.drzewo = temp3->D;
		lista_wierzcholkow.push_back(temp3);
	}
	if (temp2 == 0) {
		Wierzcholek *temp3 = new Wierzcholek();
		temp3->W = t.p_init;
		temp3->D = lista_wierzcholkow[a]->D;
		t.drzewo = temp3->D;
		lista_wierzcholkow.push_back(temp3);
	}
	if (temp2 == 1 && temp == 1) {
		if (lista_wierzcholkow[a]->D < lista_wierzcholkow[b]->D) {
			for (int i = 0; i < lista_wierzcholkow.size(); i++) {
				if (lista_wierzcholkow[i]->D == lista_wierzcholkow[b]->D) {
					lista_wierzcholkow[i]->D = lista_wierzcholkow[a]->D;
				}

			}
			t.drzewo = lista_wierzcholkow[a]->D;
			return 1;
		}
		else if (lista_wierzcholkow[a]->D > lista_wierzcholkow[b]->D) {
			for (int i = 0; i < lista_wierzcholkow.size(); i++) {
				if (lista_wierzcholkow[i]->D == lista_wierzcholkow[a]->D) {
					lista_wierzcholkow[i]->D = lista_wierzcholkow[b]->D;
				}
			}
			t.drzewo = lista_wierzcholkow[b]->D;
			return 1;
		}
		
	}


	if (temp2 == 0 || temp == 0) {
		return 1;
	}
	return 0;
}

void kruskal(Graf M) {
	int x = M.liczba_k;
	int j = 1;
	int i = 0;
	int droga = 0;
	Krawedz* temp = new Krawedz();
	Krawedz* temp2 = new Krawedz();
	temp = lista_krawedzi[0];
	for (i = 0; i < M.liczba_k; i++)
	{

		temp = lista_krawedzi[i];
		if (listawput(*temp) == 1) {
			lista_krawedzi_krusk.push_back(temp);
		}
	}
	
	
	//	for (int t = 0; t < lista_krawedzi_krusk.size(); t++)
//		{
	//		cout << "Krawedzie: " << t << " Waga:" << lista_krawedzi_krusk[t]->waga << " | Wierzcholek A: " << lista_krawedzi_krusk[t]->p_init << "  B:  " << lista_krawedzi_krusk[t]->p_final << endl;
//			droga = lista_krawedzi_krusk[t]->waga + droga;
//		}
//	cout << " Calkowity koszt polaczen: " << droga << endl;
	M_D = 0;
}

bool unikalny(Krawedz k)
{
	for (int i = 0; i < lista_prim.size(); i++) {
		if (lista_prim[i]->numerkr == k.numerkr) {
			return 0;
		}
	}
		return 1;
}

bool listap(Krawedz t)
{
	int temp = 0;
	int temp2 = 0;
	if (lista_wierzcholkow_p.empty() != 1) {
		for (int i = 0; i < lista_wierzcholkow_p.size(); i++) {
			if (lista_wierzcholkow_p[i]->W == t.p_final && unikalny(t)==0) {
				temp = 1;
			}
			if (lista_wierzcholkow_p[i]->W == t.p_init && unikalny(t) == 0) {
				temp2 = 1;
			}
		}
	}
	if (temp == 0) {
		Wierzcholek *temp3 = new Wierzcholek();
		temp3->W = t.p_final;
		lista_wierzcholkow_p.push_back(temp3);
	}
	if (temp2 == 0) {
		Wierzcholek *temp3 = new Wierzcholek();
		temp3->W = t.p_init;
		lista_wierzcholkow_p.push_back(temp3);
	}
	if (temp2 == 0 || temp == 0) {
		return 1;
	}
	return 0;
}

bool listap2(Krawedz t)
{
	int temp = 0;
	int temp2 = 0;
	if (lista_wierzcholkow_prim.empty() != 1) {
		for (int i = 0; i < lista_wierzcholkow_prim.size(); i++) {
			if (lista_wierzcholkow_prim[i]->W == t.p_final) {

				temp = 1;
			}
			if (lista_wierzcholkow_prim[i]->W == t.p_init) {
				temp2 = 1;
			}
		}
	}
	if (temp == 0 && temp2==1) {
		Wierzcholek *temp3 = new Wierzcholek();
		temp3->W = t.p_final;
	}
	if (temp2 == 0 && temp==1) {
		Wierzcholek *temp3 = new Wierzcholek();
		temp3->W = t.p_init;
	}
	if (temp2 == 0 || temp == 0) {
		return 1;
	}
	return 0;
}

bool listap3(Krawedz t)
{
	int temp = 0;
	int temp2 = 0;
	if (lista_wierzcholkow_prim.empty() != 1) {
		for (int i = 0; i < lista_wierzcholkow_prim.size(); i++) {
			if (lista_wierzcholkow_prim[i]->W == t.p_final) {
				temp = 1;
			}
			if (lista_wierzcholkow_prim[i]->W == t.p_init) {
				temp2 = 1;
			}
		}
	}
	if (temp == 0 && temp2 == 1) {
		Wierzcholek *temp3 = new Wierzcholek();
		temp3->W = t.p_final;
		lista_wierzcholkow_prim.push_back(temp3);
	}
	if (temp2 == 0 && temp == 1) {
		Wierzcholek *temp3 = new Wierzcholek();
		temp3->W = t.p_init;
		lista_wierzcholkow_prim.push_back(temp3);
	}
	if (temp2 == 0 || temp == 0) {
		return 1;
	}
	return 0;
}


void prim(Graf M)
{
	int flaga = 0;
	Krawedz* temp = new Krawedz();
	temp = lista_krawedzi[0];
	Wierzcholek* W = new Wierzcholek();
	W->W = temp->p_init;
	lista_wierzcholkow_prim.push_back(W);
	while (lista_wierzcholkow_prim.size() != M.liczba_w)
	{
		if (lista_wierzcholkow_prim.size() != 0) {

			for (int j = 0; j < lista_krawedzi.size();j++) {
				if (lista_krawedzi[j]->p_init == W->W)
				{
					if (listap(*lista_krawedzi[j]) == 1) {
						lista_prim.push_back(lista_krawedzi[j]);
					}

				}
				if (lista_krawedzi[j]->p_final == W->W)
				{
					if (listap(*lista_krawedzi[j]) == 1) {
						lista_prim.push_back(lista_krawedzi[j]);
					}

				}
			}
		}

		else {
			lista_prim.push_back(lista_krawedzi[0]);
		}

		int t = 0;
		flaga = 1;
		int Waga = MAX + 1;
		if (lista_prim2.size() != 0) {
			for (int k = 0; k < lista_prim.size(); k++) {
				flaga = 1;
				if (lista_prim[k]->waga < Waga == 1) {
					if (listap2(*lista_prim[k]) == 1) {
						Waga = lista_prim[k]->waga;
						t = k;
					}
				}

			}
		}
		else { Waga = lista_prim[t]->waga; }

		flaga = 0;
		int flaga2 = 0;
		lista_prim2.push_back(lista_prim[t]);

		Wierzcholek* tw = new Wierzcholek();
		if (lista_wierzcholkow_prim.size() != 0) {
			if (listap3(*lista_prim[t]) == 1) {
				tw->W = lista_wierzcholkow_prim[lista_wierzcholkow_prim.size() - 1]->W;
			}
			W = tw;
		}
	}
	int droga = 0;/*
	for (int t = 0; t < lista_prim2.size(); t++)
	{
		cout << "Krawedzie: " << t << " Waga:" << lista_prim2[t]->waga << " | Wierzcholek A: " << lista_prim2[t]->p_init << "  B:  " << lista_prim2[t]->p_final << endl;
		droga = lista_prim2[t]->waga + droga;

	}
	cout << " Calkowity koszt polaczen: " << droga << endl;
	*/
}

int main()
{
	int wybor;
	cout << "  1. Skorzystaj z pliku (domyslny 'Plik')\n  2. Wylosuj na podstawie wielkosc i gestosci. \n \t";
	cin >> wybor;
	srand(time(NULL));
	Graf M;
	double x, y, z;
	int a = 0, b = 0, c = 0;
	if (wybor == 1) {
		a = M.wczytaj();
	//	M.liczk(b);
		M.liczba_k = lista_krawedzi.size();
		M.wyswietl();
	}
	if (wybor == 2) {
		cout << "Wielkosc:" << endl;
		cin >> a;
		cout << "Podaj gestosc: (25+)\n";
		while (b < 25) {
			cin >> b;
		}
		M.liczba_w = a;
	//	M.liczk(b);
		M.tablica();
		M.liczk(b);
		M.zeruj();
		M.wypelnij();
		cout << "Wyswietlic?\n (1)";
		cin >> wybor;
		if (wybor == 1)
			M.wyswietl();
		wybor = 0;
		cout << "Zapisac Graf?\n (1)";
		cin >> wybor;
		if (wybor == 1)
			M.zapisz();
	}
	quick2(0, lista_krawedzi.size() - 1);

	system("pause");

	cout << "Licze metoda Prima..." << endl;
	y = obliczSekundy(clock());
	prim(M);
	x = obliczSekundy(clock());
	z = x - y;
	cout << endl << z << endl;


	system("pause");
	quick2(0, lista_krawedzi.size() - 1);
	cout << "Licze metoda Kruskala..." << endl;
	y = obliczSekundy(clock());
	kruskal(M);
	x = obliczSekundy(clock());
	z = x - y;
	cout << endl << z << endl;
	system("pause");

	return 0;
	
}

